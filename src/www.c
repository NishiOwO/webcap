#include <webcap/www.h>
#include <webcap/url.h>
#include <webcap/util.h>
#include <webcap.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define WRITE(sock, str) write(sock, str, strlen(str))

struct webcap_response* webcap_www_parse(struct webcap_url* url){
	struct webcap_response* response = malloc(sizeof(*response));

	struct hostent* hp;

	response->http_status_text = NULL;	
	response->http_body = NULL;
	response->http_headers = NULL;
	response->status = WEBCAP_SUCCESS;
	response->error = -1;
	if((hp = gethostbyname(url->host)) == NULL){
		response->status = WEBCAP_ERROR;
		response->error = WEBCAP_ERROR_GETHOSTBYNAME;
		return response;
	}else{
		struct sockaddr_in h;
		memset(&h, 0, sizeof(h));
		h.sin_family = AF_INET;
		h.sin_port = htons(url->port);
		h.sin_addr = *(struct in_addr*)hp->h_addr_list[0];
		int sock;
	       	if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
			response->status = WEBCAP_ERROR;
			response->error = WEBCAP_ERROR_SOCKET;
			goto clean;
		}
		if(connect(sock, (struct sockaddr*)&h, sizeof(h)) < 0){
			response->status = WEBCAP_ERROR;
			response->error = WEBCAP_ERROR_CONNECT;
			goto clean;
		}
		if(strcmp(url->protocol, "http") == 0){
			WRITE(sock, "GET ");
			WRITE(sock, url->path);
			WRITE(sock, " HTTP/1.1\r\n");
			WRITE(sock, "Host: ");
			WRITE(sock, url->host);
			WRITE(sock, "\r\n\r\n");
			char* buf = malloc(1);
			buf[0] = 0;
			int i = 0;
			while(1){
				buf = realloc(buf, i + 2);
				buf[i+1] = 0;
				read(sock, buf+i, 1);
				i++;
				if(buf[i - 1] == ' ' || (buf[i - 1] == '\n')){
					buf[i - 1] = 0;
					i--;
					break;
				}
			}
			if(strlen(buf) == strlen("HTTP/0.0")){
				char* version = sstrdup(buf);
				version[5] = '0';
				version[7] = '0';
				if(strcmp(version, "HTTP/0.0") == 0){
					response->http_version = atof(buf + 5);
					response->http_status_code = 0;
					i = 0;
					while(1){
						buf = realloc(buf, i + 2);
						buf[i+1] = 0;
						read(sock, buf+i, 1);
						i++;
						if(buf[i - 1] == ' ' || (buf[i - 1] == '\n')){
							buf[i - 1] = 0;
							i--;
							break;
						}else{
							response->http_status_code *= 10;
							response->http_status_code += buf[i - 1] - '0';
						}
					}
					i = 0;
					response->http_status_text = malloc(1);
					response->http_status_text[0] = 0;
					response->http_headers = malloc(sizeof(*response->http_headers));
					response->http_headers[0] = NULL;
					while(1){
						response->http_status_text = realloc(response->http_status_text, i + 2);
						response->http_status_text[i+1] = 0;
						read(sock, response->http_status_text+i, 1);
						i++;
						if(response->http_status_text[i - 1] == '\r'){
							response->http_status_text[i - 1] = 0;
							i--;
							break;
						}
					}
					i = 0;
					char newline = 0;
					response->http_headers_count = 0;
					while(1){
						buf = realloc(buf, i + 2);
						buf[i+1] = 0;
						if(newline){
							char ch = buf[i];
							read(sock, buf+i, 1);
							buf[i] = ch;
							newline = 0;
							continue;
						}
						read(sock, buf+i, 1);
						i++;
						if(buf[i - 1] == '\r'){
							if(i == 1){
								read(sock, buf, 1);
								break;
							}else{
								buf[i - 1] = 0;
								i--;
								int j;
								for(j = 0; j < strlen(buf); j++){
									if(buf[j] == ':'){
										if(buf[j + 1] == ' '){
											int k;
											for(k = j + 1; k < strlen(buf); k++){
												buf[k] = buf[k + 1];
											}
											buf[k] = 0;
											buf[k - 1] = 0;
										}
										break;
									}
								}
								char* header = sstrdup(buf);
								header[j] = 0;
								struct webcap_pair* pair = malloc(sizeof(*pair));
								pair->key = sstrdup(header);
								pair->value = sstrdup(header + j + 1);
								response->http_headers = realloc(response->http_headers, sizeof(*response->http_headers) * (response->http_headers_count + 2));
								response->http_headers[response->http_headers_count] = pair;
								response->http_headers[response->http_headers_count + 1] = NULL;
								response->http_headers_count++;
								free(header);
								i = 0;
								newline = 1;
							}
						}
					}
					uint64_t length = 0;
					for(i = 0; response->http_headers[i] != NULL; i++){
						if(strcmp(response->http_headers[i]->key, "Content-Length") == 0){
							printf("%s\n", response->http_headers[i]->value);
							length = atol(response->http_headers[i]->value);
							break;
						}
					}
					response->http_body = malloc(length + 1);
					response->http_body[length] = 0;
					read(sock, response->http_body, length);
					response->status = WEBCAP_HTTP;
				}
				free(version);
			}
			free(buf);
		}
		printf("\nhi\n");
		close(sock);
clean:
		return response;
	}
}

void webcap_www_free(struct webcap_response* response){
	if(response->http_status_text != NULL) free(response->http_status_text);
	if(response->http_body != NULL) free(response->http_body);
	if(response->http_headers != NULL){
		int i;
		for(i = 0; response->http_headers[i] != NULL; i++){
			free(response->http_headers[i]->key);
			free(response->http_headers[i]->value);
			free(response->http_headers[i]);
		}
		free(response->http_headers);
	}
	free(response);
}
