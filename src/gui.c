#include <webcap/gui.h>
#include <webcap/util.h>
#include <webcap/core.h>
#include <webcap.h>

#include <string.h>
#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/RowColumn.h>
#include <Xm/MessageB.h>
#include <Xm/Text.h>
#include <Xm/TextF.h>
#include <Xm/PushB.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/Separator.h>
#include <Xm/ScrolledW.h>
#include <Xm/ScrollBar.h>
#include <Xm/DrawingA.h>

#include <X11/Intrinsic.h>
#include <X11/X.h>

extern int argc;
extern char** argv;

char* webcap_help_items[] = {
	"About",
	"Test"
};

Widget startup;
Widget* webcap_widget_list;
struct webcap_fonts fonts;

void webcap_jump_call(Widget w, int item_no){
	Arg arg[2];
	char* url = XmTextFieldGetString(webcap_url);
	webcap_core_render(&webcap_widget_list, webcap_web, url, &fonts);
}

void webcap_help_call(Widget w, int item_no){
	printf("%s\n", webcap_help_items[item_no]);
	if(strcmp(webcap_help_items[item_no], "About") == 0){
		Arg args[3];
		char* str = sstrdup("EEE++ WebCap v");
		char* tmp;
	
		tmp = str;
		str = sstrcat(tmp, WEBCAP_VERSION);
		free(tmp);
	
		tmp = str;
		str = sstrcat(tmp, "\nCopyright (C) 2023 EEE++ tEam.");
		free(tmp);
	
		XmString xm_string = XmStringCreateLocalized(str);
		XtSetArg(args[0], XmNmessageString, xm_string);
		XtSetArg(args[1], XmNlabelFontList, fonts.menu);
		XtSetArg(args[2], XmNbuttonFontList, fonts.menu);
		Widget webcap_about_dialog = XmCreateInformationDialog(webcap_top_window, "info", args, 3);
		RemoveButton(webcap_about_dialog, XmDIALOG_CANCEL_BUTTON);
		RemoveButton(webcap_about_dialog, XmDIALOG_HELP_BUTTON);
		XmStringFree(xm_string);
		XtManageChild(webcap_about_dialog);
	}
}

char webcap_gui_init(){
	webcap_widget_list = malloc(sizeof(*webcap_widget_list) * 4);
	webcap_widget_list[3] = 0;
	Arg arg[16];
	webcap_top_window = XtVaAppInitialize(&webcap_app, "EEE++ WebCap", NULL, 0, &argc, argv, NULL, NULL);

	XFontStruct* font;
	XmFontListEntry entry;
	font = XLoadQueryFont(XtDisplay(webcap_top_window), "-adobe-times-bold-r-normal-*-24-*-*-*-*-*-iso8859-1");
	entry = XmFontListEntryCreate("header1-2-3", XmFONT_IS_FONT, font);
	fonts.header1 = XmFontListAppendEntry(NULL, entry);
	fonts.header2 = XmFontListAppendEntry(NULL, entry);
	fonts.header3 = XmFontListAppendEntry(NULL, entry);
	XtFree((char*)entry);
	font = XLoadQueryFont(XtDisplay(webcap_top_window), "-adobe-times-bold-r-normal-*-18-*-*-*-*-*-iso8859-1");
	entry = XmFontListEntryCreate("header4-5-6", XmFONT_IS_FONT, font);
	fonts.header4 = XmFontListAppendEntry(NULL, entry);
	fonts.header5 = XmFontListAppendEntry(NULL, entry);
	fonts.header6 = XmFontListAppendEntry(NULL, entry);
	XtFree((char*)entry);
	font = XLoadQueryFont(XtDisplay(webcap_top_window), "-adobe-times-medium-r-normal-*-15-*-*-*-*-*-iso8859-1");
	entry = XmFontListEntryCreate("font", XmFONT_IS_FONT, font);
	fonts.font = XmFontListAppendEntry(NULL, entry);
	XtFree((char*)entry);
	font = XLoadQueryFont(XtDisplay(webcap_top_window), "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-iso8859-1");
	entry = XmFontListEntryCreate("menu", XmFONT_IS_FONT, font);
	fonts.menu = XmFontListAppendEntry(NULL, entry);
	XtFree((char*)entry);

	webcap_window = XtVaCreateManagedWidget("Main", xmMainWindowWidgetClass, webcap_top_window, NULL);
	XtResizeWidget(webcap_top_window, 500, 500, 1);
	XmString help_label = XmStringCreateLocalized("Help");
	XmString jump_label = XmStringCreateLocalized("Jump");
	webcap_menubar = XmVaCreateSimpleMenuBar(
			webcap_window, "Jump",
			NULL
	);
	XmStringFree(jump_label);
	XmString about_label = XmStringCreateLocalized("About WebCap");
	webcap_help = XtVaCreateManagedWidget("Help", xmCascadeButtonWidgetClass, webcap_menubar, XmNmnemonic, 'H', NULL);
	Widget help_menu = XmVaCreateSimplePulldownMenu(webcap_menubar, "Help", 0,  (XtCallbackProc)webcap_help_call,
		XmVaPUSHBUTTON, about_label, 'A', XmNfontList, NULL, XmNbuttonFontList, fonts.menu, NULL,
	NULL);
	XmStringFree(about_label);
	XmStringFree(help_label);
	XtSetArg(arg[0], XmNmenuHelpWidget, webcap_help);
	XtSetValues(webcap_menubar, arg, 1);
	XtSetArg(arg[0], XmNfontList, fonts.menu);
	XtSetValues(webcap_help, arg, 1);
	XtSetValues(help_menu, arg, 1);
	webcap_url_label = XmCreateLabel(webcap_menubar, "URL: ", arg, 1);
	XtManageChild(webcap_url_label);
	XtSetArg(arg[0], XmNeditable, True);
	XtSetArg(arg[1], XmNfontList, fonts.menu);
	webcap_url = XmCreateTextField(webcap_menubar, "URL", arg, 2);
	XtManageChild(webcap_url);
	XtSetArg(arg[0], XmNfontList, fonts.menu);
	webcap_jump = XmCreatePushButton(webcap_menubar, "Jump", arg, 1);
	XtAddCallback(webcap_jump, XmNactivateCallback, (XtCallbackProc)webcap_jump_call, NULL);
	XtManageChild(webcap_jump);
	Widget sepv = XmVaCreateSeparator(webcap_menubar, "SepV",
		XmNorientation, XmVERTICAL,
		XmNseparatorType, XmDOUBLE_LINE,
		XmNwidth, 10,
	NULL);
	XtManageChild(sepv);
	XtSetArg(arg[0], XmNscrollingPolicy, XmAUTOMATIC);
	webcap_frame = XmCreateScrolledWindow(webcap_window, "WebWindow", arg, 1);
	XtSetArg(arg[0], XmNpacking, XmPACK_NONE);
	webcap_web = XmCreateRowColumn(webcap_frame, "Web", arg, 1);
	XtManageChild(webcap_frame);
	XtManageChild(webcap_web);
    	XtManageChild(webcap_menubar);
	webcap_widget_list[0] = XmVaCreateLabel(webcap_web, sstrcat("Welcome to EEE++ WebCap v", WEBCAP_VERSION),
		XmNx, 0,
		XmNy, 0,
		XmNfontList, fonts.header1,
		NULL
	);
	webcap_widget_list[1] = XmVaCreateLabel(webcap_web, "This is the default page.",
		XmNx, 0,
		XmNy, 2+24,
		XmNfontList, fonts.font,
		NULL
	);
	webcap_widget_list[2] = XmVaCreateLabel(webcap_web, "Type URL and press [Jump] to enter into the internet world.",
		XmNx, 0,
		XmNy, 2+2+24+15,
		XmNfontList, fonts.font,
		NULL
	);
	XtManageChild(webcap_widget_list[0]);
	XtManageChild(webcap_widget_list[1]);
	XtManageChild(webcap_widget_list[2]);
	XmTextFieldSetString(webcap_url, "ftp.jaist.ac.jp");
	XtRealizeWidget(webcap_top_window);
	XtAppMainLoop(webcap_app);
}
