#include <webcap/core.h>
#include <webcap/gui.h>
#include <webcap/util.h>
#include <webcap/url.h>
#include <webcap/www.h>
#include <webcap.h>

#include <X11/Intrinsic.h>
#include <Xm/Xm.h>
#include <Xm/Label.h>

#include <stdio.h>
#include <stdlib.h>

FILE* urandom = NULL;

extern char** argv;

void webcap_core_render(Widget** widgets, Widget web, const char* url, struct webcap_fonts* fonts){
	if(urandom == NULL){
		urandom = fopen("/dev/urandom", "r");
	}
	int i;
	for(i = 0; (*widgets)[i] != 0; i++){
		XtUnmanageChild((*widgets)[i]);
	}
	*widgets = realloc(*widgets, sizeof(**widgets));
	struct webcap_url* parsed_url = webcap_parse_url(url);
	printf("Protocol: %s\nHost: %s\nPort: %d\nPath: %s\n",
		parsed_url->protocol,
		parsed_url->host,
		parsed_url->port,
		parsed_url->path
	);
	struct webcap_response* response = webcap_www_parse(parsed_url);
	webcap_free_url(parsed_url);
	if(response->status == WEBCAP_ERROR){
		*widgets = realloc(*widgets, sizeof(**widgets) * 3);
		(*widgets)[0] = XmVaCreateLabel(web, "Error",
			XmNx, 0,
			XmNy, 0,
			XmNfontList, fonts->header1,
			NULL
		);
		if(response->error == WEBCAP_ERROR_GETHOSTBYNAME){
			(*widgets)[1] = XmVaCreateLabel(web, "WebCap couldn't resolve the hostname/host.",
				XmNx, 0,
				XmNy, 2+24,
				XmNfontList, fonts->font,
				NULL
			);
		}else if(response->error == WEBCAP_ERROR_SOCKET){
			(*widgets)[1] = XmVaCreateLabel(web, "WebCap couldn't create the socket.",
				XmNx, 0,
				XmNy, 2+24,
				XmNfontList, fonts->font,
				NULL
			);
		}else if(response->error == WEBCAP_ERROR_CONNECT){
			(*widgets)[1] = XmVaCreateLabel(web, "WebCap couldn't create the connection.",
				XmNx, 0,
				XmNy, 2+24,
				XmNfontList, fonts->font,
				NULL
			);
		}else{
			(*widgets)[1] = XmVaCreateLabel(web, "WebCap received the unknown error. This shouldn't happen.",
				XmNx, 0,
				XmNy, 2+24,
				XmNfontList, fonts->font,
				NULL
			);
		}
		XtManageChild((*widgets)[0]);
		XtManageChild((*widgets)[1]);
		(*widgets)[2] = 0;
	}else{
		if(response->status == WEBCAP_HTTP){
			*widgets = realloc(*widgets, sizeof(**widgets) * 2);
			(*widgets)[0] = XmVaCreateLabel(web, response->http_body,
				XmNx, 0,
				XmNy, 0,
				XmNfontList, fonts->font,
				NULL
			);
			XtManageChild((*widgets)[0]);
			(*widgets)[1] = 0;
		}
	}
	webcap_www_free(response);
}
