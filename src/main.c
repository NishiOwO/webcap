#include <stdio.h>

#include <webcap/gui.h>
#include <webcap.h>
#include <stdlib.h>

int argc;
char** argv;

char* sstrdup(const char*);

int main(int _argc, char** _argv){
	argc = _argc;
	argv = malloc((argc + 1) * sizeof(*argv));
	int i;
	argv[argc] = NULL;
	argv[0] = sstrdup("EEE++ WebCap");
	for(i = 1; i < argc; i++){
		argv[i] = sstrdup(_argv[i]);
	}
	printf("WebCap v%s starting up\n", WEBCAP_VERSION);
	webcap_gui_init();
}
