#include <webcap/url.h>
#include <webcap/util.h>

#include <stdlib.h>
#include <string.h>

struct webcap_url* webcap_parse_url(const char* url){
	struct webcap_url* result = malloc(sizeof(*result));
	result->host = sstrdup("");
	result->protocol = sstrdup("http");
	result->path = sstrdup("/");
	result->port = 80;
	char ch;
	char* str = malloc(4);
	memset(str, 0, 4);
	int i = 0;
	enum {
		URL_PROTOCOL = 0,
		URL_HOST,
		URL_PORT,
		URL_PATH,
	};
	int mode = URL_PROTOCOL;
	while(1){
		if(mode == URL_PROTOCOL){
			int j;
			for(j = i; j < i + 3 && url[j] != 0; j++){
				str[j - i] = url[j];
				str[j - i + 1] = 0;
			}
			if(strcmp(str, "://") == 0){
				free(result->protocol);
				result->protocol = malloc(i + 1);
				result->protocol[i] = 0;
				memcpy(result->protocol, url, i);
				mode = URL_HOST;
				i += 2;
			}
		}else if(mode == URL_HOST){
			if(url[i] == '/'){
				mode = URL_PATH;
			}else if(url[i] == ':'){
				mode = URL_PORT;
				result->port = 0;
			}else{
				char* tmp2 = malloc(2);
				tmp2[0] = url[i];
				tmp2[1] = 0;
				char* tmp = result->host;
				result->host = sstrcat(tmp, tmp2);
				free(tmp);
			}
		}else if(mode == URL_PORT){
			if(url[i] == '/'){
				mode = URL_PATH;
			}else{
				char* tmp2 = malloc(2);
				result->port *= 10;
				result->port += url[i] - '0';
			}
		}else if(mode == URL_PATH){
			char* tmp2 = malloc(2);
			tmp2[0] = url[i];
			tmp2[1] = 0;
			char* tmp = result->path;
			result->path = sstrcat(tmp, tmp2);
			free(tmp);
		}
		i++;
		if(url[i] == 0){
			if(mode == URL_PROTOCOL){
				mode = URL_HOST;
				i = 0;
			}else{
				break;
			}
		}
	}
	free(str);
	return result;
}

void webcap_free_url(struct webcap_url* url){
	if(url->host != NULL) free(url->host);
	if(url->protocol != NULL) free(url->protocol);
	if(url->path != NULL) free(url->path);
	free(url);
}
