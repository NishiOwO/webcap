#include <webcap/util.h>

#include <Xm/MessageB.h>
#include <X11/Intrinsic.h>

#include <stdlib.h>

char* sstrcat(const char* str1, const char* str2){
	char* result = malloc(strlen(str1) + strlen(str2) + 1);
	result[strlen(str1) + strlen(str2)] = 0;
	strcpy(result, str1);
	strcpy(result + strlen(str1), str2);
	return result;
}

char* sstrdup(const char* str){
	char* result = malloc(strlen(str) + 1);
	result[strlen(str)] = 0;
	strcpy(result, str);
	return result;
}

void RemoveButton(Widget w, int button){
	Widget remove;
	remove = XmMessageBoxGetChild(w, button);
	XtUnmanageChild(remove);
}
