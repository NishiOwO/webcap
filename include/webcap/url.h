#ifndef __WEBCAP_WEBCAP_URL_H__
#define __WEBCAP_WEBCAP_URL_H__

#include <stdint.h>

struct webcap_url {
	char* protocol;
	char* host;
	uint16_t port;
	char* path;
};

struct webcap_url* webcap_parse_url(const char* url);
void webcap_free_url(struct webcap_url* url);

#endif
