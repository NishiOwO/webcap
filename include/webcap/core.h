#ifndef __WEBCAP_WEBCAP_CORE_H__
#define __WEBCAP_WEBCAP_CORE_H__

#include <X11/Intrinsic.h>

#include <webcap/gui.h>

void webcap_core_render(Widget** widgets, Widget web, const char* url, struct webcap_fonts* fonts);

#endif
