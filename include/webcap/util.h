#ifndef __WEBCAP_WEBCAP_UTIL_H__
#define __WEBCAP_WEBCAP_UTIL_H__

#include <X11/Intrinsic.h>

char* sstrcat(const char* str1, const char* str2);
char* sstrdup(const char* str);
void RemoveButton(Widget w, int button);

#endif
