#ifndef __WEBCAP_WEBCAP_WWW_H__
#define __WEBCAP_WEBCAP_WWW_H__

#include <webcap/url.h>
#include <webcap.h>

struct webcap_pair {
	char* key;
	char* value;
};

struct webcap_response {
	int status;
	int error;

	/* HTTP */

	double http_version;
	int http_status_code;
	char* http_status_text;
	struct webcap_pair** http_headers;
	int http_headers_count;
	char* http_body;
};

struct webcap_response* webcap_www_parse(struct webcap_url* url);
void webcap_www_free(struct webcap_response* response);

#endif
