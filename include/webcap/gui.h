#ifndef __WEBCAP_WEBCAP_GUI_H__
#define __WEBCAP_WEBCAP_GUI_H__

#include <Xm/Xm.h>

struct webcap_fonts {
	XmFontList font;
	XmFontList menu;
	XmFontList header1;
	XmFontList header2;
	XmFontList header3;
	XmFontList header4;
	XmFontList header5;
	XmFontList header6;
};

static Widget webcap_top_window;
static Widget webcap_menubar;
static Widget webcap_window;
static Widget webcap_help;
static Widget webcap_url;
static Widget webcap_jump;
static Widget webcap_url_label;
static Widget webcap_frame;
static Widget webcap_web;
static XtAppContext webcap_app;

char webcap_gui_init();

#endif
